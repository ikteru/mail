

/*
 *cette fonction ajoute les elements a la base de données de firebase
 */
function addElement () {
    var ref = new Firebase('https://mail-1dffc.firebaseio.com/messages');
    var object = document.getElementById("objet");
    var message = document.getElementById("message");
    var etat = getCheckedRadio();
    createRow(object,message,etat);

   ref.push({
        objet : object.value,
        msg : message.value,
        eat : etat
    });

    object.value="";
    message.value="";

    var radioButtons = document.getElementById("group1");
    for (var x = 0; x < radioButtons.length; x ++) {
        if (radioButtons[x].checked) {
            radioButtons[x].checked = false;
        }
    }
}

/*
 *cette fonction selectionne tous le lignes de la table
 */
function SelectAll(ref, name) {

    var form = ref;

    while (form.parentNode && form.nodeName.toLowerCase() != 'form'){
        form = form.parentNode;
    }

    var elements = form.getElementsByTagName('input');

    for (var i = 0; i < elements.length; i++) {
        if (elements[i].type == 'checkbox' && elements[i].name == name) {
            elements[i].checked = ref.checked;
        }
    }
}


/**
 * cette fonction ajoute une ligne a la fin de la table
 */
function addRow(){
    var table = document.getElementById("myTable");
    var row = table.insertRow(-1);
    return row;
}

function getCheckedRadio() {
    var radioButtons = document.getElementsByName("group1");
    for (var x = 0; x < radioButtons.length; x ++) {
        if (radioButtons[x].checked) {
            return radioButtons[x].value;
        }
    }
}
/**
 * cette fonctionne crée les columns de la table
 */

function createRow(object, message, etat){
    var row=addRow();
    var cell1 = row.insertCell(0);
    var element1 = document.createElement("input");

    element1.type = "checkbox";
    element1.name="checkbox[]";
    cell1.appendChild(element1);
    var cell2 = row.insertCell(1);
    cell2.innerHTML = object.value;
    var cell3 = row.insertCell(2);
    cell3.innerHTML = message.value;
    var cell4 = row.insertCell(3);
   cell4.innerHTML = etat;

}


function deleteRow(){

}

/**
 * La fonction qui supprime toutes les lignes séléctionnés
 */
function deleteRow(tableID)  {
    var table = document.getElementById(tableID);
    var rowCount = table.rows.length;
    for(var i=1; i<rowCount; i++)
    {
        var row = table.rows[i];
        var chkbox = row.cells[0].childNodes[0];
        if(null != chkbox && true == chkbox.checked)
        {
            table.deleteRow(i);
            rowCount--;
            i--;
        }
    }

}
